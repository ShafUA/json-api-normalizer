function reduceJsonApiDataList(result, entry) {
  return {
    ...result,
    [entry.type]: {
      byId: {
        ...(result[entry.type] && result[entry.type].byId),
        [entry.id]: {
          id: entry.id,
          ...entry.attributes,
          ...Object.keys(entry.relationships || {}).reduce(
            (relations, key) => ({
              ...relations,
              [`${key}Id`]: entry.relationships[key].data.length
                ? entry.relationships[key].data.map(relationData => relationData.id)
                : entry.relationships[key].data.id
            }),
            {},
          )
        },
      },
      ids: ((result[entry.type] && result[entry.type].ids) || []).concat(entry.id),
    }
  };
}

export default function normalize(data) {
  return data.data.concat(data.included || []).reduce(reduceJsonApiDataList, {});
}

export const normalizeItem = (entry) => reduceJsonApiDataList({}, entry.data);
