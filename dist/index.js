"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = normalize;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function reduceJsonApiDataList(result, entry) {
  return _extends({}, result, _defineProperty({}, entry.type, {
    byId: _extends({}, result[entry.type] && result[entry.type].byId, _defineProperty({}, entry.id, _extends({
      id: entry.id
    }, entry.attributes, Object.keys(entry.relationships || {}).reduce(function (relations, key) {
      return _extends({}, relations, _defineProperty({}, key + "Id", entry.relationships[key].data.length ? entry.relationships[key].data.map(function (relationData) {
        return relationData.id;
      }) : entry.relationships[key].data.id));
    }, {})))),
    ids: (result[entry.type] && result[entry.type].ids || []).concat(entry.id)
  }));
}

function normalize(data) {
  return data.data.concat(data.included || []).reduce(reduceJsonApiDataList, {});
}

var normalizeItem = exports.normalizeItem = function normalizeItem(entry) {
  return reduceJsonApiDataList({}, entry.data);
};
